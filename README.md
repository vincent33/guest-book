# Guest Book Project

This project is a base for a recruitment exercise. It is built with Vue 3 and requires Node.js version 20.

## Installation

To install the project dependencies, follow these steps:

1. Clone the repository to your local machine.
2. Navigate to the project directory.
3. Run the following command to install the dependencies:

```
yarn install
```

## Scripts in package.json

Here is an explanation of the different scripts present in the `package.json` file:

- `yarn serve`: Starts the development server. After running this script, you can open your browser to `http://localhost:8080` to see the running application.
- `yarn build`: Creates a production build of the application. The output files will be created in the `dist` directory.
- `yarn lint`: Runs the linter on the project's source code.
- `yarn test`: Runs the project's tests.

## Running the application in development mode

To run the application in development mode, run the following command:

```
yarn serve
```

# Recruitment Exercise

## Objectif

Développement d'une application web pour afficher les messages laissés par les visiteurs dans le livre d'or d'un musée.

## Technical Constraints

- **Type de projet:** application web.
- **Technologie:** Vue.js version 3.
- **Mode de fonctionnement:** uniquement en développement, sans mise en production..

## Fonctionnalités

### Affichage des messages du livre d'or

- Création d'une page web pour visualiser les messages d'un livre d'or.
- Tous les messages doivent être affichés sur une seule page.
- Les messages sont affichés sous forme de cards incluant le nom du visiteur, la date du message, la nationalité (EN, FR, DE), la note attribuée, et le texte du message.

### Ordre des messages

- Afficher les messages par ordre chronologique, du plus récent au plus ancien.
- Un bouton pour inverser l'ordre d'affichage, du plus ancien au plus récent. (non prioritaire)

### Application bilingue FR/EN

- Possibilité de changer de langue (français/anglais) à tout moment via un bouton français et un bouton Anglais par exemple.
- Présence d'un bandeau en haut de page avec le logo et le sélecteur de langue.

### Messages favoris

- Créer une zone en haut de la page dédiée à l'affichage des messages favoris.
- Au depart, il n'y a pas de message favoris.
- Un bouton sur chaque card de message permet d'ajouter le message aux favoris sans pour autant le retirer de la liste principale.

### Affichage d'un message (non prioritaire)

- En cliquant sur un message, une modale doit afficher le message complet, occupant 80% de l'écran.
  
### Créer un nouveau message

- Possibilité d'ajouter un nouveau message via un menu accessible par un bouton situé en haut à droite
- Redirection vers une nouvelle page avec un formulaire pour saisir un nouveau message.
- Après enregistrement, l'utilisateur est redirigé vers la page principale où le nouveau message apparaît en tête de liste.

## Gestion des données

Les messages sont stockés dans le fichier [message.json](./public/messages.json).

Pour cet exercice, la persistance des données n'est pas demandée. Une persistence temporaire ou permanante est un plus.

## Visuel

Le client a fourni une maquette de ce qu'il souhaiterait réaliser. Elle se trouve dans le fichier [maquette.pdf](./maquette.pdf).
Quelques assets sont fournis dans le dossier [assets](./src/assets/)

### Couleurs principales :

- couleur 1 : #c42d3d
- couleur 2 : 1b2135